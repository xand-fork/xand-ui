SHELL := /bin/bash

#
#	DOCKER IMAGE HANDLING
#

# Image / Container Data
NAME := xand-ui
CONTAINER_NAME := $(NAME)
FULL_NAME := gcr.io/xand-dev/$(NAME)
VERSION := $(shell cat ./xand-ui/package.json | jq -r .version)
PORT := 3000

# Possible Image:Tag combinations
VERSION_TAG := $(FULL_NAME):$(VERSION)
DEV_VERSION_TAG := $(VERSION_TAG)-dev
LATEST_TAG := $(FULL_NAME):latest
DEV_TAG := $(FULL_NAME):dev
CUSTOM_TAG := ${FULL_NAME}:${DOCKER_TARGET}

# DOCKER_TARGET
# Variable used in Docker build and publish commands to determine what name:tag combo/s to operate on
# Valid values:
# 	VERSION 		- Uses FULL_NAME:VERSION
#	DEV_VERSION 	- Uses FULL_NAME:VERSION-dev
#	LATEST Uses		- Uses FULL_NAME:VERSION and FULL_NAME:latest
#	DEV				- Uses FULL_NAME:VERSION-dev and FULL_NAME:dev
#	custom value	- Uses FULL_NAME:DOCKER_TARGET with given custom tag value (e.g. DOCKER_TARGET=test --> FULL_NAME:test)
DOCKER_TARGET_VERSION := VERSION
DOCKER_TARGET_DEV_VERSION := DEV_VERSION
DOCKER_TARGET_LATEST := LATEST
DOCKER_TARGET_DEV := DEV

build-docker:
	@if [[ -z "${DOCKER_TARGET}" ]]; then \
		echo 'Error: DOCKER_TARGET var not defined.  Define DOCKER_TARGET= as one of DEV, LATEST, VERSION, DEV_VERSION, or a custom tag value.'; \
		exit 1; \
	elif [[ "${DOCKER_TARGET}" = "${DOCKER_TARGET_VERSION}" ]]; then \
		echo "Building ${VERSION_TAG}"; \
		docker build -f ./deployment/Dockerfile -t ${VERSION_TAG} ./deployment/ ; \
	elif [[ "${DOCKER_TARGET}" = "${DOCKER_TARGET_DEV_VERSION}" ]]; then \
		echo "Building ${DEV_VERSION_TAG}"; \
		docker build -f ./deployment/Dockerfile -t ${DEV_VERSION_TAG} ./deployment/ ; \
	elif [[ "${DOCKER_TARGET}" = "${DOCKER_TARGET_LATEST}" ]]; then \
		echo "Building ${VERSION_TAG} and $(LATEST_TAG)"; \
		docker build -f ./deployment/Dockerfile -t ${LATEST_TAG} -t ${VERSION_TAG} ./deployment/ ; \
	elif [[ "${DOCKER_TARGET}" = "${DOCKER_TARGET_DEV}" ]]; then \
		echo "Building $(DEV_VERSION_TAG) and $(DEV_TAG)"; \
		docker build -f ./deployment/Dockerfile -t ${DEV_TAG} -t ${DEV_VERSION_TAG} ./deployment/ ; \
	else \
		echo "Building $(CUSTOM_TAG)"; \
		docker build -f ./deployment/Dockerfile -t ${CUSTOM_TAG} ./deployment/ ; \
	fi

run-docker: kill-docker
	@if [[ -z "${TAG}" ]]; then \
		echo 'Error: TAG var not defined.  Define TAG= as the tag of the ${FULL_NAME} docker image to run.'; \
		exit 1; \
	fi
	docker run -it --rm --name $(CONTAINER_NAME) -p ${PORT}:${PORT} ${FULL_NAME}:$(TAG)

kill-docker:
	@docker rm -f $(CONTAINER_NAME) || true

publish-docker: kill-docker build-docker
	@if [[ -z "${DOCKER_TARGET}" ]]; then \
		echo 'Error: TARGET var not defined.  Define TARGET= as one of DEV, LATEST, VERSION, DEV_VERSION, or a custom tag value.'; \
		exit 1; \
	elif [[ "${DOCKER_TARGET}" = "${DOCKER_TARGET_VERSION}" ]]; then \
		echo "Publishing ${VERSION_TAG}"; \
		docker push ${VERSION_TAG}; \
	elif [[ "${DOCKER_TARGET}" = "${DOCKER_TARGET_DEV_VERSION}" ]]; then \
		echo "Publishing ${DEV_VERSION_TAG}"; \
		docker push ${DEV_VERSION_TAG}; \
	elif [[ "${DOCKER_TARGET}" = "${DOCKER_TARGET_LATEST}" ]]; then \
		echo "Publishing ${VERSION_TAG} and $(LATEST_TAG)"; \
		docker push ${VERSION_TAG}; \
		docker push ${LATEST_TAG}; \
	elif [[ "${DOCKER_TARGET}" = "${DOCKER_TARGET_DEV}" ]]; then \
		echo "Publishing $(DEV_VERSION_TAG) and $(DEV_TAG)"; \
		docker push ${DEV_VERSION_TAG}; \
		docker push ${DEV_TAG}; \
	else \
		echo "Publishing $(CUSTOM_TAG)"; \
		docker push ${CUSTOM_TAG}; \
	fi

#
#      KUBERNETES HANDLING
#

# k8s directories
K8S_DIR := deployment/kube
K8S_BASE := ${K8S_DIR}/base
K8S_OVERLAYS := ${K8S_DIR}/overlays
K8S_COMPILED := ${K8S_DIR}/compiled
K8S_TMP := ${K8S_OVERLAYS}/tmp

# Config Constants from deployment/base
K8S_NAMESPACE := xand-ui
K8S_DEPLOYMENT_NAME := xand-ui
K8S_SERVICE_NAME := storybook-service

# K8S_TARGET
# Variable used in build-k8s to determine what overlay to use for kustomize
# Valid values:
#	custom value	- Uses deployment/overlays/${K8S_TARGET}
K8S_TARGET := local

# Other build-k8s Vars
# Output filename for generated yaml
OUTPUT := deployment.yaml
# File used to apply in apply-k8s
K8S_APPLY_FILE := deployment.yaml

# KUBERNETES SCRIPTS

# Builds a deployment.yaml k8s file from kustomize and environment variables given an overlay
# Example: make build-k8s K8S_TARGET=local IMAGE_TAG=alpha
# ---> Where IMAGE_TAG is an env variable that is set to be used in the local overlay config file
build-k8s:
	@cd deployment/kube && ./build_k8s.sh ${K8S_TARGET}

# Packages all necessary files for remote k8s kustomize creation and compilation
# Example: make package-k8s K8S_TARGET=local IMAGE_TAG=alpha
# ---> Where IMAGE_TAG is an env variable that is set to be used in the local overlay config file
package-k8s:
	@cd deployment/kube && ./package_template.sh ${K8S_TARGET} ${K8S_TARGET}

apply-k8s:
	@echo "Applying file ${K8S_COMPILED}/${K8S_APPLY_FILE}"
	@kubectl apply -f ${K8S_COMPILED}/${K8S_APPLY_FILE}

delete-k8s:
	@echo "Delete namespace ${K8S_NAMESPACE} and service ${K8S_SERVICE_NAME}"
	@kubectl delete namespace ${K8S_NAMESPACE}

status-k8s:
	@kubectl describe deployment ${K8S_DEPLOYMENT_NAME} -n ${K8S_NAMESPACE}
