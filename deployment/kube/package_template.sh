#!/bin/bash
#
# package_template.sh
#
# Script for packaging all files necessary to run script and build deployment yaml from a template
# Usage: ./package_template {KUSTOMIZE_OVERLAY_FOLDER_NAME} {PACKAGE_NAME}
#   $1: folder name of template in overlays/
#   $2: (Optional) filename for .tar.gz package.  Will create {PACKAGE_NAME}.{VERSION}.tar.gz if supplied.
#       Example: ./package_template local local-test will move files to package/local and also create a package/local-test.(VERSION).tar.gz file
#

# Constants
DIR="."
BASE_DIR="$DIR/base"
OVERLAYS_DIR="$DIR/overlays"
PACKAGE_DIR="$DIR/package"

# IO
TARGET_NAME="$1"
TARGET_DIR="$OVERLAYS_DIR/$TARGET_NAME"
OUTPUT_DIR="$PACKAGE_DIR/$TARGET_NAME"
PACKAGE_NAME="$2"
OUTPUT_LOCAL="package/$TARGET_NAME"

function validate() {
    if [[ -z $1 ]]
    then
        echo "Error: No overlay given."
        exit 1
    elif [[ ! -d "$TARGET_DIR" ]]
    then
        echo "Error: $TARGET_DIR directory does not exist."
        exit 1
    fi
    VERSION=$(cat ${TARGET_DIR}/VERSION)
    if [[ -z "$VERSION" ]]
    then
        echo "Error: No VERSION file found in ${TARGET_DIR}"
        exit 1
    fi
}

function package() {
    rm -rf "$OUTPUT_DIR"
    mkdir -p "$OUTPUT_DIR/overlays"
    echo -e "\tpackage/$TARGET_NAME" created
    cp -rp "$BASE_DIR" "$OUTPUT_DIR/base"
    echo -e "\tbase copied to package/$TARGET_NAME/base"
    cp -rp "$TARGET_DIR" "$OUTPUT_DIR/overlays/$TARGET_NAME"
    echo -e "\toverlays/$TARGET_NAME copied to package/overlays/$TARGET_NAME"
    cp -p "build_k8s.sh" "$OUTPUT_DIR"
    echo -e "\tbuild_k8s.sh script copied to package/$TARGET_NAME/build_k8s.sh"
    if [[ ! -z "$PACKAGE_NAME" ]]
    then
        tar --exclude "**.tar.gz" -czf "./$PACKAGE_NAME.$VERSION.tar.gz" "./$OUTPUT_LOCAL"
        mv "./$PACKAGE_NAME.$VERSION.tar.gz" "./package/$PACKAGE_NAME.$VERSION.tar.gz"
        echo -e "\tpackage/$PACKAGE_NAME.$VERSION.tar.gz created"
    fi
}

validate $@
echo -e "\nPackaging template and scripts for ${TARGET_DIR} (v$VERSION) to $OUTPUT_LOCAL"
package
