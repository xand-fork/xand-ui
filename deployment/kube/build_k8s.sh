#!/bin/bash
#
# build_k8s.sh
#
# Script for replacing placeholder values in kustomize configurations with
# supplied defaults or environment variables
#
# Usage: ./build_k8s {KUSTOMIZE_OVERLAY_FOLDER_NAME} {OUTPUT_FILENAME}
#   First arg: directory in overlays to use for the replacement (e.g. local)
#   Second arg: output filename (e.g. deployment.yaml) - will default to $1.yaml if not supplied
#
# Workflow:
# 1. Reads config file of given overlay
# 2. Runs string replacement for given key-value pairs in vars
# 3. Stores resulting kustomization files in /tmp
# 4. Runs kustomize on the new files in /tmp and builds to /compiled
#

# Constants
DIR="."
BASE_DIR="$DIR/base"
OVERLAYS_DIR="$DIR/overlays"
OUTPUT_DIR="$DIR/compiled"
TMP_DIR="$DIR/tmp"

TARGET_NAME="$1"
TARGET_DIR="$OVERLAYS_DIR/$1"
OUTPUT="$OUTPUT_DIR/${2:-"deployment.yaml"}"
CONFIG="$TARGET_DIR/config"

declare -A replacements
declare -A replacement_source

function validate() {
    if [[ -z $1 ]]
    then
        echo "Error: No overlay given."
        exit 1
    elif [[ ! -d "$TARGET_DIR" ]]
    then
        echo "Error: $TARGET_DIR directory does not exist."
        exit 1
    fi
    VERSION=$(cat ${TARGET_DIR}/VERSION)
    if [[ -z "$VERSION" ]]
    then
        echo "Error: No VERSION file found in ${TARGET_DIR}"
        exit 1
    fi
}

function load_default_config() {
    echo -e "\nReading $CONFIG for keys and default values:"
    while IFS='=' read -r k v || [ -n "$k" ]
    do
        replacements["$k"]="$v"
        replacement_source["$k"]="Default"
        echo -e "\t$k"
        echo -e "\t\tDefault: $v"
    done < "$CONFIG"
}

function read_env_vars() {
    echo -e "\nChecking environment variables for key values:"
    for k in "${!replacements[@]}"
    do
        echo -e "\tChecking $k"
        if [[ ! -z "${!k}" ]]
        then
            echo -e "\t\tFound: ${!k}"
            replacements["$k"]="${!k}"
            replacement_source["$k"]="Env Variable"
        else
            echo -e "\t\tN/A"
        fi
    done
}

function output_final_replacements() {
    echo -e "\nReplacements configured:"
    for k in "${!replacements[@]}"
    do
        echo -e "\t$k"
        echo -e "\t\t${replacements["$k"]} (${replacement_source["$k"]})"
    done
}

function move_files_to_tmp() {
    echo -e "\nReplacing values and preparing template for build"
    rm -rf "$TMP_DIR"
    mkdir -p "$TMP_DIR/overlays"
    cp -r "$BASE_DIR" "$TMP_DIR/base"
    cp -r "$TARGET_DIR" "$TMP_DIR/overlays/$TARGET_NAME"
}

function replace_values() {
    for k in "${!replacements[@]}"
    do
        echo -e "\tReplacing $k with ${replacements["$k"]}"
        find "$TMP_DIR" -iname '*.yaml' -exec sed -i "s@#$k@${replacements["$k"]}@g" {} \;
    done
}

function compile() {
    echo -e "\nCompiling and saving to $OUTPUT"
    rm -f "$OUTPUT"
    mkdir -p "$OUTPUT_DIR"
    kustomize build "${TMP_DIR}/overlays/${TARGET_NAME}" > "$OUTPUT"
    rm -rf "$TMP_DIR"
}

validate $@
echo -e "\nRunning variable replacement on ${TARGET_DIR} (v$VERSION)"
load_default_config
read_env_vars
output_final_replacements
move_files_to_tmp
replace_values
compile