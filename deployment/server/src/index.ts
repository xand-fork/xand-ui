import express from 'express';
import path from 'path';

const publicPath = path.resolve(__dirname, '../public/');

const app = express();
const port = 3000;

app.use(express.static(publicPath));
app.get('/', (_req, res) => res.sendFile(path.resolve(publicPath, 'index.html')));

// eslint-disable-next-line no-console
app.listen(port, () => console.log(`Server started listening to port ${port}`));
