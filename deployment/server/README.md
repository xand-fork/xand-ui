# Actions

`yarn install` will install all dependencies
`yarn build-client` will build a new storybook website for the server to work with in `public/`

#### General

- `yarn clean` - Clean all temp files
- `yarn lint` - Lint the project
- `yarn fix` - Lint and autofix

#### Building

- `yarn build-client` builds website and places in `public/`
- `yarn build` builds server to `dist`

#### Running

- `yarn start-dev:watch` - starts in dev mode with a watcher on typescript files
- `yarn start-dev` - starts in dev mode
- `yarn start` - builds and starts in prod mode