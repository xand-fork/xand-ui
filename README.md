###  TODO

- Finalize some components as examples (Typography -- Text, Paragraph, Heading; Logo, Icon, Grid)
    - https://reactjs.org/docs/forwarding-refs.html 
    - https://reactjs.org/docs/hooks-reference.html#usememo

# Project License

MIT OR Apache-2.0


# Dependencies:

- node / npm
- yarn
- docker
- kubectl
- kustomize (separate from kubectl)
- minikube (if running local k8s deployments)
- jq (for json parsing in command line)
- tar (for packaging k8s build scripts)

# What's This?

`xand-ui` is a UI Library for common xand-branded frontend elements for use internally. WIP.  For installation and development instructions, see [The Xand-UI Library Readme](xand-ui/README.md)

Versioning scheme is based on `xand-ui/package.json` version and should be manually incremented as necessary.

# Versioning

This project version is based on `xand-ui/package.json` which uses semver. When the version is referred to in documentation or code, that is the version it is referencing.

However, the k8s configuration directory is versioned separately for independent deployment. That version is maintained in `deployment/kube/overlays/{overlay}/VERSION`.  It should be manually handled using semver conventions much like the `xand-ui/package.json` version.  This will allow changes in any k8s deployment overlay to be detected and published/versioned without relying on xand-ui's version being bumped unnecessarily.  There is a gap for the base not being versioned, but that's ok for now.

# Structure

- `ci/`
    - Gitlab pipeline yaml files
- `deployment/`
    - [`Dockerfile`](deployment/Dockerfile) for the storybook deployment server
    - `kube/`
        - Kubernetes configuration files and scripts for working with `kubectl` and `kustomize` to deploy
    - [`server/`](deployment/server/README.md)
        - Small express.js server for serving static storybook site
- [`xand-ui`](xand-ui/README.md)
    - Xand UI library

# [Makefile Commands](Makefile)

## Docker Commands

**NOTE**: Docker build/publish commands use a `DOCKER_TARGET` variable to determine what image tag/s to use (e.g. `make build-docker DOCKER_TARGET=LATEST`):

Valid `DOCKER_TARGET` values: 

- "LATEST"                
    - produces `:latest` and `:{VERSION}` tags
- "DEV"                   
    - produces `:dev` and `:{VERSION}-dev` tags
- "VERSION"               
    - produces `:{VERSION}` tag
- "DEV_VERSION"           
    - produces `:{VERSION}-dev` tag
- A custom tag value    
    - produces `:{DOCKER_TARGET}` tag

Commands: 

- `make build-docker` - Builds docker image/s with tag/s based on `DOCKER_TARGET`
- `make publish-docker` - Publishes docker image/s to `gcr.io/xand-dev/xand-ui` based on `DOCKER_TARGET` value
- `make kill-docker` - Forces a removal of `xand-ui` container
- `make run-docker` - Kills any running `xand-ui` container and runs `gcr.io/xand-dev/xand-ui:${TAG}` in container name `xand-ui`
    - Requires a `TAG` variable input (e.g. `make run-docker TAG=1.0.0`)

## Kubernetes Commands

**NOTE**: Kubernetes build command uses a `K8S_TARGET` variable to determine what kube overlay folder to use for kustomize config generation (e.g. `make build-k8s K8S_TARGET=local`).  

Valid `K8S_TARGET` values: 

- a custom folder name      
    - uses deployment/overlays/${K8S_TARGET}

e.g.: `make build-k8s K8S_TARGET=local IMAGE_TAG=alpha` will create a dev deployment with `gcr.io/xand-dev/xand-ui:alpha` as the container image in the configuration because that is how the `local` kustomization overlay is set up with its `config` file replacement value defaults.

To create new deployments, add an overlay folder and run the build command with `K8S_TARGET={foldername}` (or update the makefile and create new built-in deployments).  *Deployment configurations should be kept in source code if possible*.

Commands: 

- `make build-k8s` - Builds a k8s config file to `deployment/kube/compiled` based on `K8S_TARGET`
    - By default, saves the file as 'deployment.yaml', but can be overridden by setting `OUTPUT=(filename)` for `deployment/kube/compiled/(filename).yaml`.
- `make apply-k8s` -  Applies the k8s file `deployment/kube/compiled/deployment.yaml` to running cluster
    - Optionally, define `{K8S_APPLY_FILE}`, which will be used instead of `deployment.yaml`.
        - e.g. `make apply-k8s K8S_APPLY_FILE=release_deployment.yaml`
- `make delete-k8s` - Deletes the `xand-ui` namespace and `storybook-service` service from running cluster
- `make status-k8s` - Prints the status of the `xand-ui` deployment in running cluster

You'll run into problems running the base k8s cluster locally, as it needs to be authenticated to use `gcr.io/xand-dev` images.  To get around this, and to debug local builds, see [using k8s configuration locally](#using-k8s-configuration-locally) about how to run from locally-built docker images.

**NOTE**: For `xand-ui` it won't typically be necessary to run the Docker image or k8s configurations locally, as you'll likely mean to be running the `xand-ui` storybook typescript project directly from source or from `dist/` using the package.json scripts.

# Publishing and Deployment

## Xand UI NPM Package

- Master will automatically publish `xand-ui` npm package with version given in `xand-ui/package.json`
    - `xand-ui` source must change to trigger this
- Any non-master branch can publish a `xand-ui@{VERSION}-beta.{CI_JOB_ID}` npm package tagged with `beta` by running a custom publish job available in the pipeline

## Storybook UI Explorer Webserver Docker Image

- Master will automatically publish `gcr.io/xand-dev/xand-ui:latest` and `gcr.io/xand-dev/xand-ui:{VERSION}` docker images when merged
    -  `xand-ui` or `deployment/server` must change for this to trigger
- Any `gcr.io/xand-dev/xand-ui` tag/s supported by the makefile can be run from a manual pipeline job in the publish stage called `publish-docker-dev`
    - **You will need to supply a `DOCKER_TARGET` variable in the gitlab manual CI job trigger page before running it**
- Local and manual building / publishing can be done via makefile commands

## Kubernetes for Storybook UI Explorer

### Generating a k8s configuration

By default, there are two deployments: `local` and `ingress`.  Their configurations are stored in `overlays/*/` dirs.  They use a basic load balancer defined in `base` and are appropriate for using with new clusters or locally.  New overlays will be added over time for different target environments.

To create a k8s configuration for another deployment, add the deployment as a kustomize overlay to `deployments/overlays/`.  Deployments should be kept in source.  If you need a unique deployment for a one-time use, build a configuration from an existing deployment and use the generated yaml file as a base to create your own.  You'll need a `VERSION` file to version the deployment (deployments are versioned by overlay) and a `config` file to supply replacement value defaults for any environment variables you want to use.

The `build-k8s` script in `kube/` will replace any `#{VARIABLE_NAME}` strings in kustomize files with values in either the overlay's `config` file or taken from environment variables of the same name.  See the `local` overlay for the basic example.

### Deploying generated k8s configuration

**NOTE:** Make sure the environment that is going to run the cluster is authenticated for `gcr.io/xand-dev`. After that, you should be able to use the makefile commands to launch a deployment.

- Master automatically generates a release kubernetes yaml and pushes it to `tpfs.octopus.app` private repository for artifacts
    - It versions this release on `deployment/kube/ingress/VERSION` and should be maintained based on semver
    - It only does this if a file in `deployment/kube/` has changed
    - It bases this publish on the `release` deployment overlay
    - Pushed to octopus via CI as `xand-ui.{VERSION}.tar.gz`

### Using k8s configuration locally

If you are trying to run k8s configs locally, you'll need to build new docker images using minikube's docker process for the configured containers to use (for authentication reasons):

```bash
eval $(minikube docker-env) # Sets the current terminal to use minikube's docker environment

# Any docker makefile build commands here
make build-docker DOCKER_TARGET=DEV 
... 

eval $(minikube docker-env -u) # Sets the current terminal back to the user's docker environment when finished (Minikube must still be running)
```

Here is a full example of testing a k8s configuration locally:

```bash
minikube start
eval $(minikube docker-env)
make build-docker DOCKER_TARGET=DEV_VERSION
eval $(minikube docker-env -u)
make build-k8s K8S_TARGET=DEV_VERSION
make apply-k8s
make status-k8s
minikube service storybook-service -n xand-ui # opens storybook
```
