import { addons } from '@storybook/addons';

import xandTheme from './xand-theme';

addons.setConfig({
  theme: xandTheme,
});
