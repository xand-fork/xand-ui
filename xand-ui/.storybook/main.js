const path = require('path');

module.exports = {
  stories: ['../src/**/*.stories.tsx'],
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-notes"
  ],
  webpackFinal: async (config, { configType }) => {

    // https://stackoverflow.com/questions/58757104/react-storybook-not-displaying-svg-icons
    const assetRule = config.module.rules.find(rule => rule.test && rule.test.test('.svg'));
    const assetLoader = {
      loader: assetRule.loader,
      options: assetRule.options || assetRule.query,
    };

    config.module.rules.unshift(
    {
      test: /\.svg$/,
      use: ['@svgr/webpack', assetLoader],
    });
    return config;
  },
}