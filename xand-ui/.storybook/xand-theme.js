import { create } from '@storybook/theming/create';

export default create({
  base: 'light',
  brandTitle: 'Xand-UI',
  brandUrl: 'https://gitlab.com/TransparentIncDevelopment/r-d/xand-ui',
});
