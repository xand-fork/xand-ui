import XandTheme from '../models';
import Colors from '../../../constants/colors';

const defaultTheme: XandTheme = {
  primary: Colors.primary.gold[100],
  seconary: Colors.primary.brown[100],
  tertiary: Colors.secondary.orange,
  accent: Colors.primary.red[100],
  action: Colors.secondary.green,
  text: Colors.primary.black[100],
  textInverted: Colors.primary.white[100],
  black: Colors.primary.black[100],
  white: Colors.primary.white[100],
};

export default defaultTheme;

// TODO: Use and refine
