import React from 'react';

import XandBaseStyle from './XandBaseStyle';
import XandFontFace from './XandFontFaces';

export default function XandGlobalStyles(): JSX.Element {
  return (
    <>
      <XandBaseStyle />
      <XandFontFace />
    </>
  );
}
