import { createGlobalStyle } from 'styled-components';

import TelegrafRegularWoff from '../../fonts/Telegraf-Regular.woff';
import TelegrafRegularWoff2 from '../../fonts/Telegraf-Regular.woff2';
import TelegrafRegularTtf from '../../fonts/Telegraf-Regular.ttf';

import TelegrafLightWoff from '../../fonts/Telegraf-Light.woff';
import TelegrafLightWoff2 from '../../fonts/Telegraf-Light.woff2';
import TelegrafLightTtf from '../../fonts/Telegraf-Light.ttf';

import TelegrafMediumWoff from '../../fonts/Telegraf-Medium.woff';
import TelegrafMediumWoff2 from '../../fonts/Telegraf-Medium.woff2';
import TelegrafMediumTtf from '../../fonts/Telegraf-Medium.ttf';

import TelegrafSemiBoldWoff from '../../fonts/Telegraf-SemiBold.woff';
import TelegrafSemiBoldWoff2 from '../../fonts/Telegraf-SemiBold.woff2';
import TelegrafSemiBoldTtf from '../../fonts/Telegraf-SemiBold.ttf';

const XandFontFaces = createGlobalStyle`
@font-face {
  font-family: 'Telegraf';
  src: url(${TelegrafLightWoff2}) format('woff2'),
  url(${TelegrafLightWoff}) format('woff'),
  url(${TelegrafLightTtf}) format('truetype');
  font-weight: 300;
  font-style: normal;
  font-display: swap;
}
@font-face {
  font-family: 'Telegraf';
  src: url(${TelegrafRegularWoff2}) format('woff2'),
  url(${TelegrafRegularWoff}) format('woff'),
  url(${TelegrafRegularTtf}) format('truetype');
  font-weight: 400;
  font-style: normal;
  font-display: swap;
}
@font-face {
  font-family: 'Telegraf';
  src: url(${TelegrafMediumWoff2}) format('woff2'),
  url(${TelegrafMediumWoff}) format('woff'),
  url(${TelegrafMediumTtf}) format('truetype');
  font-weight: 500;
  font-style: normal;
  font-display: swap;
}
@font-face {
  font-family: 'Telegraf';
  src: url(${TelegrafSemiBoldWoff2}) format('woff2'),
  url(${TelegrafSemiBoldWoff}) format('woff'),
  url(${TelegrafSemiBoldTtf}) format('truetype');
  font-weight: 600;
  font-style: normal;
  font-display: swap;
}
`;

export default XandFontFaces;
