import React from 'react';
import styled, { FlattenSimpleInterpolation, StyledComponent } from 'styled-components';

import TypographyStyles from './styles';

type HTagVariant = 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6';
type BodyVariant = 'p';
type ComponentVariant = HTagVariant | BodyVariant;
type XandTypographyVariant = 'jumbo' | HTagVariant | 'body-large' | 'body' | 'body-small';

const xandTypographyVariantMapping: Record<XandTypographyVariant, { component: ComponentVariant, style: FlattenSimpleInterpolation }> = {
  jumbo: { component: 'h1', style: TypographyStyles.jumboCss },
  h1: { component: 'h1', style: TypographyStyles.h1Css },
  h2: { component: 'h2', style: TypographyStyles.h2Css },
  h3: { component: 'h3', style: TypographyStyles.h3Css },
  h4: { component: 'h4', style: TypographyStyles.h4Css },
  h5: { component: 'h5', style: TypographyStyles.h5Css },
  h6: { component: 'h6', style: TypographyStyles.h6Css },
  'body-large': { component: 'p', style: TypographyStyles.largeBodyCss },
  body: { component: 'p', style: TypographyStyles.bodyCss },
  'body-small': { component: 'p', style: TypographyStyles.smallBodyCss },
};

export interface XandTypographyProps {
  className?: string;
  children?: React.ReactNode;
  variant: XandTypographyVariant,
}

function chooseComponent(variant: XandTypographyVariant): StyledComponent<ComponentVariant, Record<string, unknown>, Record<string, unknown>, never> {
  return styled(xandTypographyVariantMapping[variant].component)`
    ${xandTypographyVariantMapping[variant].style}
  `;
}

export default function XandTypography(props: XandTypographyProps): JSX.Element {
  const {
    className, children, variant, ...rest
  } = props;

  const StyledXandTypography = chooseComponent(variant);

  return (
    <StyledXandTypography
      className={className}
      variant={variant}
      {...rest}
    >
      {children}
    </StyledXandTypography>
  );
}
