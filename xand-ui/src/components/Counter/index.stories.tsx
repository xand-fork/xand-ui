import React from 'react';
import { Meta, Story } from '@storybook/react';

import Counter from '.';

export default {
  component: Counter,
  title: 'Counter',
} as Meta;

export const DemoCounter: Story<void> = () => <Counter />;
