import React from 'react';
import { Meta, Story } from '@storybook/react';

import XandLogo, { XandLogoProps } from '.';

export default {
  component: XandLogo,
  title: 'XandLogo',
} as Meta;

export const ConfigurableLogo: Story<XandLogoProps> = (args) => <XandLogo {...args} />;
