export default {
  primary: {
    red: {
      100: '#EC4625',
      60: '#F4907C',
      15: '#FCE3DE',
      5: '#FEF6F4',
    },

    black: {
      100: '#0C2026',
      60: '#6D797D',
      15: '#DBDEDE',
      5: '#F3F4F4',
    },

    white: {
      100: '#E5EAED',
      0: '#000000',
    },

    brown: {
      100: '#4B401F',
      60: '#938C79',
      15: '#E4E2DD',
      5: '#F6F5F4',
    },

    gold: {
      100: '#968041',
      60: '#C0B38D',
      15: '#EFECE2',
      5: '#FAF9F5',
    },
  },
  secondary: {
    green: '#5CBC63',
    lightGreen: '#55F1A6',
    blue: '#5A5AE5',
    lightBlue: '#48FFE4',
    purple: '#653E49',
    pink: '#F95D82',
    red: '#EC4625',
    orange: '#FF9422',
    yellow: '#FCD42D',
    lightYellow: '#F0FF55',
  },
};
