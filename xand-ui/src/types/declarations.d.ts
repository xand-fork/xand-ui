declare module '*.svg' {
    // for SVGR to use SVGs as React Components
    // https://stackoverflow.com/questions/54121536/typescript-module-svg-has-no-exported-member-reactcomponent
    export const ReactComponent: React.FC<React.SVGProps<SVGSVGElement>>;
    const src: string;
    export default src;
}

declare module '*.png';
declare module '*.jpg';
declare module '*.woff';
declare module '*.woff2';
declare module '*.ttf';
declare module '*.pdf';
declare module '*.scss';
declare module '*.css';
