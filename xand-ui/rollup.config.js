import path from 'path';
import commonjs from '@rollup/plugin-commonjs';
import image from '@rollup/plugin-image';
import svgr from '@svgr/rollup';
import json from '@rollup/plugin-json';
import url from '@rollup/plugin-url';
import postcss from 'rollup-plugin-postcss';
import peerDepsExternal from 'rollup-plugin-peer-deps-external';
import resolve from '@rollup/plugin-node-resolve';
import babel from '@rollup/plugin-babel';

const config = require('./package.json');

export default {
  input: 'src/index.ts',
  output: [
    {
      file: config.main,
      format: 'cjs',
      sourcemap: true,
      exports: 'named',
    },
    {
      file: config.module,
      format: 'es',
      sourcemap: true,
      exports: 'named',
    },
  ],
  external: [/@babel\/runtime/, 'react', 'react-dom'],
  plugins: [
    resolve({
      extensions: ['.js', '.jsx', '.tsx', '.ts'],
    }),
    svgr(),
    image({ exclude: '*.svg' }),
    json(),
    url({
      include: ['**/*.woff', '**/*.woff2', '**/*.ttf', '**/*.gif', '**/*.jpg', '**/*.png'],
      limit: Infinity,
    }),
    commonjs(),
    postcss(),
    peerDepsExternal(),
    // https://github.com/rollup/plugins/tree/master/packages/babel
    babel({
      babelHelpers: 'runtime',
      configFile: path.resolve(__dirname, '.babelrc'),
      exclude: 'node_modules/**',
      include: ['src/**/*'],
      extensions: ['.js', '.jsx', '.tsx', '.ts'],
    }),
  ],
};
